#!/usr/bin/perl
use strict;
use warnings;

use Test::More;
use Test::Warn;

use lib 'tests/perl_tests/lib';
use File::Temp;
use FileSlurping 'slurp';

sub tempdir {
    my $tempdir   = File::Temp->newdir( CLEANUP => $ENV{KEEP_ALL} ? 0 : 1 );
    # diag "using temp dir $tempdir";
    return $tempdir;
}

{  
    my $tempdir = tempdir();
    `./lca.R --countsFile t/fixtures/out.counts --statsOutFile ${tempdir}/stats.out >/dev/null 2>&1`;
    ok( ! $?, 'lca.R should do simple run without error' );
}

{  
    my $tempdir = tempdir();
    `./lca.R --countsFile t/fixtures/out.counts >/dev/null 2>&1`;
    ok( $?, 'lca.R should require a statsOutFile argument' );
}

{  
    my $tempdir = tempdir();
    `./lca.R --statsOutFile ${tempdir}/stats.out >/dev/null 2>&1`;
    ok( $?, 'lca.R should require a countsFile argument' );
}

{  
    my $tempdir = tempdir();
    `./lca.R --countsFile t/fixtures/out.counts --statsOutFile ${tempdir}/stats.out >/dev/null 2>&1`;
    ok( -e "${tempdir}/stats.out", 'lca.R should write out statsOutFile' );
}

done_testing;
