#!/usr/bin/perl
use strict;
use warnings;

use Test::More;
use Test::Warn;

use lib 'tests/perl_tests/lib';
use File::Temp;
use FileSlurping 'slurp';
use File::Slurp;

sub run_with(@) {
    system $^X, '../baysic.pl', @_;
    ok( ! $?, 'baysic.pl ran ok' );
}

sub tempdir {
    my $tempdir   = File::Temp->newdir( CLEANUP => $ENV{KEEP_ALL} ? 0 : 1 );
    # diag "using temp dir $tempdir";
    return $tempdir;
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --countsOutFile ${tempdir}/combined.cts --vcfOutFile ${tempdir}/combined.vcf > /dev/null 2>&1`;
    ok( $?, 'baysic.pl should require vcf argument' );
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --countsOutFile ${tempdir}/combined.cts --vcf ./t/fixtures/small.vcf --vcfOutFile ${tempdir}/combined.vcf > /dev/null 2>&1`;
    ok( $?, 'baysic.pl should require at least 2 vcf arguments' );
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/small.vcf --countsOutFile ${tempdir}/combined.cts --vcfOutFile ${tempdir}/combined.vcf > /dev/null 2>&1`;
    ok( $?, 'baysic.pl should give error message when only given one input file' );
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/small.vcf --vcf ./t/fixtures/small.vcf --vcfOutFile ${tempdir}/combined.vcf > /dev/null 2>&1`;
    ok( $?, 'baysic.pl should require countsOutFile argument' );
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --countsOutFile ${tempdir}/combined.cts --vcf ./t/fixtures/small.vcf --pvalCutoff 0.9 --vcf ./t/fixtures/small.vcf --vcfOutFile ${tempdir}/combined.vcf >${tempdir}/stdout_stderr.output 2>&1`;
    my $errors = `grep -i 'Unknown option: pvalCutoff' ${tempdir}/stdout_stderr.output`;
    ok( $errors eq '', 'baysic.pl should allow pvalCutoff argument' );
}

{  
    my $tempdir = tempdir();
    `cd ${tempdir}/ && $ENV{PWD}/baysic.pl --statsOutFile ${tempdir}/combined.stats --countsOutFile ${tempdir}/combined.cts --vcf $ENV{PWD}/./t/fixtures/small.vcf --pvalCutoff 0.9 --vcf $ENV{PWD}/./t/fixtures/small.vcf --vcfOutFile ${tempdir}/combined.vcf >${tempdir}/stdout_stderr.output 2>&1`;
    ok( ! $?, "baysic.pl should find lca.R when baysic.pl is called from a different directory" );
}

{  
    my $tempdir = tempdir();
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/merge-test-a.vcf --vcf ./t/fixtures/merge-test-b.vcf --countsOutFile ${tempdir}/combined.cts > /dev/null 2>&1`;
    ok( $?, 'baysic.pl should require vcfOutFile argument' );
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/merge-test-a.vcf --vcf ./t/fixtures/merge-test-b.vcf --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file`; # shell dependency, yuck. 
    ok( -s $counts_output_file, "baysic.pl should output a counts file" ); # -s means file has non-zero size
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/small.vcf --vcf ./t/fixtures/small.vcf --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file`; 
    ok( -s "${vcf_output_file}.pos", "baysic.pl should output a non-zero sized positions file to --vcfOutFile + .pos" ); # -s means file has non-zero size
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/merge-test-a.vcf --vcf ./t/fixtures/merge-test-b.vcf --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file --pvalCutoff 0.0`; 
    die "can't find ${vcf_output_file}.pos!!!" unless -s "${vcf_output_file}.pos";
    open my $FH, "${vcf_output_file}.pos" || die "Can't read pos file: ${vcf_output_file}.pos: $!";
    my @lines = <$FH>;
    @lines = sort @lines;
    open my $CFH, "t/fixtures/testForUniqPos.pos " || die "Can't read correct pos file: t/fixtures/testForUniqPos.pos: $!";
    my @correctLines = <$CFH>;
    @correctLines = sort @correctLines;
    ok( scalar(@lines) == 5002, "baysic.pl should report correct number of unique positions in --vcfOutFile + .pos" );
    ok( @correctLines ~~ @lines, "baysic.pl should report correct unique positions in --vcfOutFile + .pos" );
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --vcf ./t/fixtures/small.vcf --vcf ./t/fixtures/small.vcf --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file`; # shell dependency, yuck
    ok( -s $vcf_output_file, "baysic.pl should output a combined vcf file" ); # -s means file has non-zero size
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --pvalCutoff 1.0 --vcf ./t/fixtures/small.vcf --vcf ./t/fixtures/small.vcf --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file > /dev/null 2>&1`;
    my $fileContentLength = `grep -v \# $vcf_output_file`;
    ok( $fileContentLength eq '', "baysic.pl should not emit any non-header lines when given a pvalue cutoff of 1.0" );
}

{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --pvalCutoff 1.0 --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file --vcf ./t/fixtures/CosmicMutantExport_v63_300113_NOT_UNIQ_TRUNC.vcf --vcf ./t/fixtures/CosmicMutantExport_v63_300113_NOT_UNIQ_TRUNC.vcf --vcf ./t/fixtures/small.vcf >${tempdir}/stderr_stdout.output 2>&1`;
    my $postProbErrors = `grep -l "couldn't find postProb" ${tempdir}/stderr_stdout.output`;
    ok( $postProbErrors eq '', "baysic.pl should deal with VCFs with duplicate positions (resulting in contingencies like 2100 or 0231)" );
}

# check for correct merging of files
{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --pvalCutoff 0.0 --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file --vcf ./t/fixtures/merge-test-a.vcf --vcf ./t/fixtures/merge-test-b.vcf --vcf ./t/fixtures/merge-test-c.vcf >${tempdir}/stderr_stdout.output 2>&1`;
    # sorry about the following, but I can't do a straight-up diff, and I can't slurp files and 
    # compare strings because the source will always be different for these two files
    # so, I'm stuck with this kind of hack:
    my $differences = `cut -f1,2 $vcf_output_file  ./t/fixtures/merge-test-out.vcf | sort | uniq -u | grep -v '##source' | grep -v "##fileformat"`;
    ok( $differences eq "", "baysic.pl should correctly merge VCF files" );
}

# check for correct overlap counts
{  
    my $tempdir = tempdir();
    my $vcf_output_file = "${tempdir}/combined.vcf";
    my $counts_output_file = "${tempdir}/combined.cts";
    `./baysic.pl --statsOutFile ${tempdir}/combined.stats --pvalCutoff 0.0 --vcfOutFile $vcf_output_file --countsOutFile $counts_output_file --vcf ./t/fixtures/merge-test-a.vcf --vcf ./t/fixtures/merge-test-b.vcf --vcf ./t/fixtures/merge-test-c.vcf >${tempdir}/stderr_stdout.output 2>&1`;
    my $differences = `diff $counts_output_file t/fixtures/overlap_test_correct.counts`;
    ok( $differences eq "", "baysic.pl should correctly count overlaps" );
}

done_testing;
